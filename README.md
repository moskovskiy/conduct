# Conduct GraphQL server

## List of avaliable queries:

### Single user
```
query {
  user (id:"oMkWbY79876Pqwtd") {
    id
    name
    surname
    agentLM
    agentLMName
    dateLM
    agentCR
    agentCRName
    dateCR
  }
}
```
### All users preview
```
query {
	users {
    id
    name
  }
}
```
### Check login already taken
```
query {
  checkLoginExists (login:"alex") {
    exists
    id
  }
}
```
### Single document
```
query {
  document (id:"uJLA_SXOJksRSH9c") {
    id
    name
    content
    agentLM
    agentLMName
    dateLM
    agentCR
    agentCRName
    dateCR
  }
}
```
### All documents preview
```
query {
	documents {
    id
    name
    contentpreview
    dateLM
  }
}
```
## List of avaliable mutations:
### User
```

mutation {
  createUser(input: {login: "alex", name:"Artemy", surname: "Lebedev"})
}

mutation {
  updateUser(input: {id:"7PFFL2_UCvdMO8Av", login: "alex", name:"Artemy", surname: "Lebedev"})
}

mutation {
  deleteUser(id: "7PFFL2_UCvdMO8Av")
}

```
### Document
```

mutation {
  createDocument(input: {name:"Документ", content: "Содержание документа здесь"})
}

mutation {
  updateDocument(input: {id:"7PFFL2_UCvdMO8Av", name:"Новое название", content: "Новое содержание документа здесь"})
}

mutation {
  deleteDocument(id: "U5twAwI2mjlhBMtp")
}

```

