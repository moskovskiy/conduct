package main

import (
	"github.com/go-chi/chi"
	"github.com/rs/cors"
	"graphql/graph"
	"graphql/graph/generated"
	"log"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)

const defaultPort = "8080"

func corsh(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		h(w, r)
	}
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	router := chi.NewRouter()
	handler := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		Debug:            true,
	}).Handler)

	router.Handle("/api", handler)
	router.Handle("/graphql", playground.Handler("GraphQL playground", "/api"))

	log.Println("API Gateway is up!")
	log.Fatal(http.ListenAndServe(":"+port, router))

	/*
			config := gql.Config{Resolvers: &gql.Resolver{}}
			handler := handler.GraphQL(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

			http.Handle("/query", cors(handler))
			srv := handler.NewDefaultServer()

			router := chi.NewRouter()

			router.Use(cors.New(cors.Options{
				AllowedOrigins:   []string{"http://localhost:8081"},
				AllowCredentials: true,
				Debug:            true,
			}).Handler)

		    srv.AddTransport(&transport.Websocket{
		        Upgrader: websocket.Upgrader{
		            CheckOrigin: func(r *http.Request) bool {
		                 return r.Host == "example.org"
		            },
		            ReadBufferSize:  1024,
		            WriteBufferSize: 1024,
		        },
		    })

			http.Handle("/graphql", playground.Handler("GraphQL playground", "/api"))
			http.Handle("/api", corsh(srv.handler))
			http.Handle("/api", srv)

			log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)

			err := http.ListenAndServe(":"+defaultPort, router)
			if err != nil {
				panic(err)
			}
			log.Fatal(http.ListenAndServe("localhost:"+port, nil))*/
}
