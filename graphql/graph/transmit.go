package graph

import (
	"encoding/json"
	nats "github.com/nats-io/nats.go"
	"log"
	"sync"
	"time"
)

const _DebugEnabled = true//false
const defaultUserID = "7PFFL2_UCvdMO8Av"

func getServer() *nats.Conn {
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		log.Fatal(err)
	}
	return nc
}

func toJSON(in map[string]interface{}) (out string) {
	jsonString, err := json.Marshal(in)
	if err != nil {
		log.Println(err)
	}

	out = string(jsonString[:len(jsonString)])
	return out
}
func fromJSON(in []byte) (out map[string]interface{}) {
	out = make(map[string]interface{})
	_ = json.Unmarshal(in, &out)
	return out
}

func transmitJSON (nc* nats.Conn, request string, JSONi string) (JSONo string) {
	//payload["__ABSTRACT_SERVICE_action"] = request
	//payload["__ABSTRACT_SERVICE_agentId"] = agentID

	wg := sync.WaitGroup{}
	wg.Add(1)

	msg, err := nc.Request(request, []byte(JSONi), 3*time.Second)

	if err != nil {
		log.Println("Debug> NATSActions.go> ERROR! transmitJSON completed with error = ", err)
		wg.Done()
		return "{}"
	}

	if msg != nil {
		JSONo := string(msg.Data[:])

		if _DebugEnabled {
			log.Println("Debug> NATSActions.go> transmitJSON ", request, "ended with result = ", JSONo)
		}

		wg.Done()
		return JSONo
	}

	wg.Wait()
	return "{}"
}

func transmit(nc *nats.Conn, agentID string, request string, payload map[string]interface{}) (response map[string]interface{}) {
	payload["__ABSTRACT_SERVICE_action"] = request
	payload["__ABSTRACT_SERVICE_agentId"] = agentID

	transmit := toJSON(payload)

	if _DebugEnabled {
		log.Println("Debug> NATSActions.go> transmiting", request, " with payload = ", transmit, "\nJSON = ", payload)
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	msg, err := nc.Request(request, []byte(transmit), 3*time.Second)

	if err != nil {
		log.Println("Debug> NATSActions.go> ERROR! transmit completed with error = ", err)
		wg.Done()
		return make(map[string]interface{})
	}

	if msg != nil {
		result := fromJSON(msg.Data)

		if _DebugEnabled {
			log.Println("Debug> NATSActions.go> transmit ", request, "ended with result = ", result)
		}

		wg.Done()
		return result
	}

	wg.Wait()
	return make(map[string]interface{})
}
