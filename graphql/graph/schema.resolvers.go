package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"graphql/graph/generated"
	"graphql/graph/model"
	"log"
)

func (r *mutationResolver) EimCommand(ctx context.Context, name string, payload string) (string, error) {
	NATSServer := getServer()
	defer NATSServer.Close()

	result := transmitJSON(NATSServer, name, payload)
	log.Println("EIMCommand ", name, "(", payload, ") executed with result = ", result)
	return result, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input model.CreateUserFields) (string, error) {
	payload := make(map[string]interface{})

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "createUser"
	payload["login"] = input.Login
	payload["name"] = input.Name
	payload["surname"] = input.Surname

	server := getServer()
	defer server.Close()

	testingAction := "eim.User.Command.Create"
	transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	return "CreateUser", nil
}

func (r *mutationResolver) UpdateUser(ctx context.Context, input model.UpdateUserFields) (string, error) {
	payload := make(map[string]interface{})

	payload["userID"] = input.ID
	payload["login"] = input.Login
	payload["name"] = input.Name
	payload["surname"] = input.Surname

	server := getServer()
	defer server.Close()

	testingAction := "eim.User.Command.Update"
	transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	return "UpdateUser", nil
}

func (r *mutationResolver) DeleteUser(ctx context.Context, id string) (string, error) {
	payload := make(map[string]interface{})

	if id == "" {
		return "IDZERO", nil
	}

	payload["userID"] = id

	server := getServer()
	defer server.Close()

	testingAction := "eim.User.Command.Delete"
	transmit(server, defaultUserID, testingAction, payload)

	return "DeleteUser", nil
}

func (r *mutationResolver) CreateDocument(ctx context.Context, input model.CreateDocumentFields) (string, error) {
	payload := make(map[string]interface{})

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "createDocument"
	payload["name"] = input.Name
	payload["content"] = input.Content

	server := getServer()
	defer server.Close()

	testingAction := "eim.Document.Command.Create"
	transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	return "CreateDocument", nil
}

func (r *mutationResolver) UpdateDocument(ctx context.Context, input model.UpdateDocumentFields) (string, error) {
	payload := make(map[string]interface{})

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "updateDocument"

	if input == *new(model.UpdateDocumentFields) {
		return "IDZERO", nil
	}

	payload["documentID"] = input.ID
	payload["content"] = input.Content
	payload["name"] = input.Name

	server := getServer()
	defer server.Close()

	testingAction := "eim.Document.Command.Update"
	transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	return "UpdateDocument", nil
}

func (r *mutationResolver) DeleteDocument(ctx context.Context, id *string) (string, error) {
	payload := make(map[string]interface{})

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "deleteDocument"

	payload["documentID"] = id

	server := getServer()
	defer server.Close()

	testingAction := "eim.Document.Command.Delete"
	transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	return "DeleteDocument", nil
}

func (r *queryResolver) EimQuery(ctx context.Context, name string, payload string) (string, error) {
	NATSServer := getServer()
	defer NATSServer.Close()

	result := transmitJSON(NATSServer, name, payload)
	log.Println("EIMQuery ", name, "(", payload, ") executed with result = ", result)
	return result, nil
}

func (r *queryResolver) Users(ctx context.Context) ([]*model.UserPreview, error) {
	payload := make(map[string]interface{})

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "selectUsers"

	server := getServer()
	defer server.Close()

	testingAction := "eim.User.Query.GetAllNames"
	result := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	users := result["users"].([]interface{})
	counter := 0
	for range users {
		counter++
	}

	resuld := make([]*model.UserPreview, counter)

	for index, element := range users {
		log.Println("res = ", element, index)

		doc := new(model.UserPreview)

		resID := new(string)
		raw := ""
		raw, ok := element.(map[string]interface{})["id"].(string)
		if ok {
			*resID = raw
		}
		doc.ID = resID

		resName := new(string)
		raw = ""
		raw, ok = element.(map[string]interface{})["name"].(string)
		if ok {
			*resName = raw
		}
		doc.Name = resName

		resuld[index] = doc
	}

	return resuld, nil
}

func (r *queryResolver) User(ctx context.Context, id string) (*model.User, error) {
	payload := make(map[string]interface{})
	resuld := new(model.User)

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "selectUser"

	payload["id"] = id

	server := getServer()
	defer server.Close()

	testingAction := "eim.User.Query.GetById"
	result := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	// -------
	// Querying real user name by ID for last modified agent ID

	testingAction = "eim.User.Query.GetById"
	userRequest := make(map[string]interface{})

	raw := ""
	raw, ok := result["agentLM"].(string)
	if ok {
		userRequest["id"] = raw
	}

	responseNameLM := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, userRequest)
	log.Println("Debug> Resolvers.go> SCHEMA merge at", userRequest["id"], "with result =", responseNameLM)
	resagentLMName := new(string)
	raw = ""
	raw, ok = responseNameLM["name"].(string)
	if ok {
		*resagentLMName = raw
	}

	raw, ok = responseNameLM["surname"].(string)
	if ok {
		*resagentLMName = raw + " " + *resagentLMName
	}
	(*resuld).AgentLMName = resagentLMName

	// -------
	// Querying real user name by ID for created agent ID

	raw = ""
	raw, ok = result["agentCR"].(string)
	if ok {
		userRequest["id"] = raw
	}

	responseNameCR := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)
	log.Println("Debug> Resolvers.go> SCHEMA merge at", userRequest["id"], "with result =", responseNameLM)

	resagentCRName := new(string)
	raw = ""
	raw, ok = responseNameCR["name"].(string)
	if ok {
		*resagentCRName = raw
	}

	raw, ok = responseNameLM["surname"].(string)
	if ok {
		*resagentCRName = raw + " " + *resagentCRName
	}
	(*resuld).AgentCRName = resagentCRName

	// ------

	resID := new(string)
	*resID = id
	(*resuld).ID = resID

	resName := new(string)
	raw = ""
	raw, ok = result["name"].(string)
	if ok {
		*resName = raw
	}
	(*resuld).Name = resName

	resSurname := new(string)
	raw = ""
	raw, ok = result["surname"].(string)
	if ok {
		*resSurname = raw
	}
	(*resuld).Surname = resSurname

	resLogin := new(string)
	raw = ""
	raw, ok = result["login"].(string)
	if ok {
		*resLogin = raw
	}
	(*resuld).Login = resLogin

	resdateLM := new(string)
	raw = ""
	raw, ok = result["dateLM"].(string)
	if ok {
		*resdateLM = raw
	}
	(*resuld).DateLm = resdateLM

	resagentLM := new(string)
	raw = ""
	raw, ok = result["agentLM"].(string)
	if ok {
		*resagentLM = raw
	}
	(*resuld).AgentLm = resagentLM

	resdateCR := new(string)
	raw = ""
	raw, ok = result["dateCR"].(string)
	if ok {
		*resdateCR = raw
	}
	(*resuld).DateCr = resdateCR

	resagentCR := new(string)
	raw = ""
	raw, ok = result["agentCR"].(string)
	if ok {
		*resagentCR = raw
	}
	(*resuld).AgentCr = resagentCR

	return resuld, nil
}

func (r *queryResolver) Documents(ctx context.Context) ([]*model.DocumentPreview, error) {
	payload := make(map[string]interface{})

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "selectDocuments"

	server := getServer()
	defer server.Close()

	testingAction := "eim.Document.Query.GetAllDocumentsPreview"
	result := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	documents := result["documents"].([]interface{})
	counter := 0
	for range documents {
		counter++
	}

	resuld := make([]*model.DocumentPreview, counter)

	for index, element := range documents {
		//log.Println("res = ", element, index)

		doc := new(model.DocumentPreview)
		doc.ID = element.(map[string]interface{})["id"].(string)
		doc.Name = element.(map[string]interface{})["name"].(string)
		doc.Contentpreview = element.(map[string]interface{})["contentpreview"].(string)
		doc.DateLm = element.(map[string]interface{})["dateLM"].(string)
		resuld[index] = doc
	}

	return resuld, nil
}

func (r *queryResolver) Document(ctx context.Context, id string) (*model.Document, error) {
	payload := make(map[string]interface{})
	resuld := new(model.Document)

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "selectDocument"

	payload["id"] = id

	server := getServer()
	defer server.Close()

	testingAction := "eim.Document.Query.GetById"
	result := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	// -------
	// Querying real user name by ID for last modified agent ID

	testingAction = "eim.User.Query.GetById"
	userRequest := make(map[string]interface{})

	raw := ""
	raw, ok := result["agentLM"].(string)
	if ok {
		userRequest["id"] = raw
	}

	responseNameLM := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, userRequest)
	log.Println("Debug> Resolvers.go> SCHEMA merge at", userRequest["id"], "with result =", responseNameLM)

	resagentLMName := *new(string)
	raw = ""
	raw, ok = responseNameLM["name"].(string)
	if ok {
		resagentLMName += raw + " "
	}

	raw, ok = responseNameLM["surname"].(string)
	if ok {
		resagentLMName += raw
	}
	(*resuld).AgentLMName = resagentLMName

	// -------
	// Querying real user name by ID for created agent ID

	raw = ""
	raw, ok = result["agentCR"].(string)
	if ok {
		userRequest["id"] = raw
	}

	responseNameCR := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)
	log.Println("Debug> Resolvers.go> SCHEMA merge at", userRequest["id"], "with result =", responseNameLM)

	resagentCRName := *new(string)
	raw = ""
	raw, ok = responseNameCR["name"].(string)
	if ok {
		resagentCRName += raw + " "
	}

	raw, ok = responseNameCR["surname"].(string)
	if ok {
		resagentCRName += raw
	}
	(*resuld).AgentCRName = resagentCRName

	// -------

	(*resuld).ID = id

	resName := new(string)
	raw = ""
	raw, ok = result["name"].(string)
	if ok {
		*resName = raw
	}
	(*resuld).Name = *resName

	resContent := new(string)
	raw = ""
	raw, ok = result["content"].(string)
	if ok {
		*resContent = raw
	}
	(*resuld).Content = *resContent

	resdateLM := new(string)
	raw = ""
	raw, ok = result["dateLM"].(string)
	if ok {
		*resdateLM = raw
	}
	(*resuld).DateLm = *resdateLM

	resagentLM := new(string)
	raw = ""
	raw, ok = result["agentLM"].(string)
	if ok {
		*resagentLM = raw
	}
	(*resuld).AgentLm = *resagentLM

	resdateCR := new(string)
	raw = ""
	raw, ok = result["dateCR"].(string)
	if ok {
		*resdateCR = raw
	}
	(*resuld).DateCr = *resdateCR

	resagentCR := new(string)
	raw = ""
	raw, ok = result["agentCR"].(string)
	if ok {
		*resagentCR = raw
	}
	(*resuld).AgentCr = *resagentCR

	return resuld, nil
}

func (r *queryResolver) CheckLoginExists(ctx context.Context, login string) (*model.LoginExists, error) {
	payload := make(map[string]interface{})
	resuld := new(model.LoginExists)

	payload["__WEBVIEW_SERVICE_currentUserId"] = defaultUserID
	payload["__WEBVIEW_SERVICE_action"] = "selectDocument"

	payload["login"] = login

	server := getServer()
	defer server.Close()

	testingAction := "eim.User.Query.CheckLoginExists"
	result := transmit(server, payload["__WEBVIEW_SERVICE_currentUserId"].(string), testingAction, payload)

	(*resuld).Exists = result["exists"].(bool)
	(*resuld).ID = result["userId"].(string)
	/*
		resName := new(string)
		raw := ""
		raw, ok := result["name"].(string)
		if ok {
			*resName = raw
		}
		(*resuld).Name = *resName

		resContent := new(string)
		raw = ""
		raw, ok = result["content"].(string)
		if ok {
			*resContent = raw
		}
		(*resuld).Content = *resContent

		resdateLM := new(string)
		raw = ""
		raw, ok = result["dateLM"].(string)
		if ok {
			*resdateLM = raw
		}
		(*resuld).DateLm = *resdateLM

		resagentLM := new(string)
		raw = ""
		raw, ok = result["agentLM"].(string)
		if ok {
			*resagentLM = raw
		}
		(*resuld).AgentLm = *resagentLM

		resagentLMName := new(string)
		raw = ""
		raw, ok = result["agentLMName"].(string)
		if ok {
			*resagentLMName = raw
		}
		(*resuld).AgentLMName = *resagentLMName

		resdateCR := new(string)
		raw = ""
		raw, ok = result["dateCR"].(string)
		if ok {
			*resdateCR = raw
		}
		(*resuld).DateCr = *resdateCR

		resagentCR := new(string)
		raw = ""
		raw, ok = result["agentCR"].(string)
		if ok {
			*resagentCR = raw
		}
		(*resuld).AgentCr = *resagentCR

		resagentCRName := new(string)
		raw = ""
		raw, ok = result["agentCRName"].(string)
		if ok {
			*resagentCRName = raw
		}
		(*resuld).AgentCRName = *resagentCRName
	*/
	return resuld, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
