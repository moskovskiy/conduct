module graphql

go 1.12

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/nats-io/nats.go v1.10.0
	github.com/rs/cors v1.7.0
	github.com/vektah/gqlparser/v2 v2.0.1
)
